//Creation of ECR repo

resource "aws_ecr_repository" "main" {
  name = var.container_name
  image_tag_mutability = var.immutable ? "IMMUTABLE" : "MUTABLE"
  
  image_scanning_configuration {
    scan_on_push = var.scan_on_push
  }
}

//Lifecycle policy

resource "aws_ecr_lifecycle_policy" "main" {
  repository = aws_ecr_repository.main.name
  
  policy     = <<EOF
  {
    "rules": [
      {
        "rulePriority": 1,
        "description": "Expire untagged images older than 90 days",
        "selection": {
          "tagStatus": "untagged",
          "countType": "sinceImagePushed",
          "countUnit": "days",
          "countNumber": 90
        },
        "action": {
          "type": "expire"
        }
      },
      {
        "rulePriority": 2,
        "description": "Only keep the most recent 100 images",
        "selection": {
          "tagStatus": "any",
          "countType": "imageCountMoreThan",
          "countNumber": 100
        },
        "action": {
          "type": "expire"
        }
      }
    ]
}

EOF  
}

//Repo policy / access permissions

resource "aws_ecr_repository_policy" "main" {
  repository = aws_ecr_repository.main.name
  policy     = <<EOF
{
    "Version": "2008-10-17",
    "Statement": [
      {
        "Sid": "ReadonlyAccess",
        "Action": [
            "ecr:GetDownloadUrlForLayer",
            "ecr:BatchGetImage",
            "ecr:BatchCheckLayerAvailability",
            "ecr:PutImage",
            "ecr:InitiateLayerUpload",
            "ecr:UploadLayerPart",
            "ecr:CompleteLayerUpload",
            "ecr:DescribeRepositories",
            "ecr:GetRepositoryPolicy",
            "ecr:ListImages",
            "ecr:DeleteRepository",
            "ecr:BatchDeleteImage",
            "ecr:SetRepositoryPolicy",
            "ecr:DeleteRepositoryPolicy"
        ],
        "Effect": "Allow",
        "Principal": 
        {
        "AWS" : "*"
        },
        "Condition": {
            "ForAnyValue:StringLike": {
                "aws:PrincipalOrgPaths": "o-6rxcoxjkd0/*/ou-rw2x-aspz5glw/*"
                      }
                  }
      },

      {
        "Sid": "FullAccess",
        "Action": [
            "ecr:BatchCheckLayerAvailability",
            "ecr:GetDownloadUrlForLayer",
            "ecr:GetRepositoryPolicy",
            "ecr:DescribeRepositories",
            "ecr:ListImages",
            "ecr:DescribeImages",
            "ecr:BatchGetImage",
            "ecr:InitiateLayerUpload",
            "ecr:UploadLayerPart",
            "ecr:CompleteLayerUpload",
            "ecr:PutImage"
        ],
        "Effect": "Allow",
        "Principal": 
        {
        "AWS" : "*"
        },
        "Condition": {
          "ForAnyValue:StringLike": {
            "aws:PrincipalOrgPaths": "o-6rxcoxjkd0/ou-rw2x-bu3gyt63/*"
          }
        }
      }

    ]
}
EOF
}
